# coding: utf-8
"""
__authors__     = 'Laura Fléron'
__contact__     = 'laura.fleron@ac-reims.fr'
__copyleft__    = 'LGPL'
__date__        = '2022-01-05'
__version__     = '1.3'

- reception radio des données du module GPS
  à partir d'un emetteur Microbit+GPS grove
- Module recepteur (carte Microbit) connecté au PC
"""
from microbit import *
import radio

# Configuration du module radio de la MicroBit
radio.config(length=251, address=1, group=1)
radio.on()

print('demarrage...')
sleep(3000)

while True :
    recu = radio.receive()

    if recu:
        trame = recu.replace('\r\n', '')
        #trame = '$' + trame
        print(trame)
