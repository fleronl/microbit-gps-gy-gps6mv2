# coding: utf-8
"""
__authors__     = 'Laura Fléron'
__contact__     = 'laura.fleron@ac-reims.fr'
__copyleft__    = 'LGPL'
__date__        = '2022-01-05'
__version__     = '1.3'

- emetteur radio GPS type : gy-gps6mv2
                            az-delivery
                            NEO-8M-0-010
- GPS grove connecté TX grove -> sur Pin0
- Tx=Pin14 non utilisé
"""

from microbit import *
import radio

# Configuration du module radio
radio.config(length=251, address=1, group=1)
radio.on()

# Configuration de la liaison série pour le module GPS
uart.init(baudrate=9600, tx=pin1, rx=pin0)
sleep(50)

# Initialisation des variables
resteTrame = ''
trame = ''
presenceTrame = False

while (not button_a.was_pressed()):

    if uart.any():

        # On récupère 50 octets du buffer de la microBit
        textes = resteTrame + uart.read(50).decode('ascii')
        # On remplace les sauts de lignes par #
        textes = textes.replace('\r\n', '#')
        trame = ''

        for carac in textes :

            # Si 'début de trame' ou dans la trame
            if (carac == '$' or presenceTrame == True) :
                trame += carac
                resteTrame = trame
                presenceTrame = True

            # Si 'fin de trame' on envoie et on réinit les valeurs
            if (carac == '#' and presenceTrame == True) :
                trame = trame[:-1] # Enleve le '#'
                trame = trame

                radio.send(trame)
                trame = ''
                resteTrame =''
                presenceTrame = False
