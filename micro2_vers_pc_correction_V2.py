# coding: utf-8
"""
__authors__     = 'Laura Fléron'
__contact__     = 'laura.fleron@ac-reims.fr'
__copyleft__    = 'LGPL'
__date__        = '2022-01-05'
__version__     = '1.3'

- traitement de la trame RMC pour afficher le point GPS
  du module sur une carte dans votre navigateur
"""

import serial
#import pynmea2
import folium
import webbrowser

fichier_carte = 'carte.html'

s = serial.Serial('COM5',115200,timeout=1)

finBoucle = False
nbVal = 1
moyLatitude2 = 0
moyLongitude2 = 0

while not(finBoucle):
    trame = []
    
    #lecture de la trame brute NMEA
    data =str(s.readline(), 'ascii')
    data = data.replace('\r\n', '')
    trame = data.split(',')
    #print(trame)
    
    if trame[0] == '$GNRMC' :
        
        Latitude = trame[3]
        Longitude = trame[5]

        LatitudeDeci = float(Latitude[-8:])/60
        Latitude2 = int(Latitude[:2]) + LatitudeDeci

        LongitudeDeci = float(Longitude[-8:])/60
        Longitude2 = int(Longitude[:3]) + LongitudeDeci
        
        moyLatitude2 = (moyLatitude2 * (nbVal-1) + Latitude2) / nbVal
        moyLongitude2 = (moyLongitude2 * (nbVal-1) + Longitude2) / nbVal
        print(f'{nbVal} : Latitude = {round(moyLatitude2, 6)} Longitude = {round(moyLongitude2, 6)}')

        nbVal += 1
       
    if nbVal > 5 :
        finBoucle = True

s.close()

carte = folium.Map(
    location=[moyLatitude2, moyLongitude2],
    zoom_start=20,
    titles='GPS')

folium.Marker(
    location=[moyLatitude2, moyLongitude2],
    tooltip="I'm here!",
    icon=folium.Icon(color='red',
                     icon='wifi',
                     icon_color='white',
                     prefix='fa',
                     angle=0),
    ).add_to(carte)

carte.save(fichier_carte)

print("Ouverture de la map.")

webbrowser.open(fichier_carte)
